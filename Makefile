#
#   Makefile    WJ114
#	summer
#

#CXX=clang++
CXX=g++-4.8

STANDARD=-std=c++11

# g++ needs this
CXX_THREAD=-pthread

INCLUDE=-I.
#INCLUDE=-I. -I/usr/local/opt/openssl/include
CXXFLAGS=-Wall -O2 $(STANDARD) $(INCLUDE) $(CXX_THREAD)

LFLAGS=
#LFLAGS=-L/usr/local/opt/openssl/lib
LIBS=-lcrypto -lssl -lz

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $<

TARGETS=summer

.PHONY: test

all: .depend $(TARGETS)

include .depend

summer: summer.o
	$(CXX) $(LFLAGS) $(CXX_THREAD) summer.o -o summer $(LIBS)

dep .depend:
	$(CXX) $(STANDARD) -M $(INCLUDE) *.cpp >.depend

clean:
	rm -f *.o *~ core $(TARGETS)

# EOB
