#! /bin/bash
#
#	build_rpm.sh
#

if [ -z "$1" ]
then
	echo "usage: build_rpm.sh release-1.2-2"
	exit 1
fi

CI_COMMIT_TAG="$1"
CI_PROJECT_NAME="summer"
CI_PROJECT_DIR=$(pwd)

VERSION=$(echo $CI_COMMIT_TAG | awk -F- '{ print $2 }')
RELEASE=$(echo $CI_COMMIT_TAG | awk -F- '{ if ($3) { print $3 } else { print 1 } }')
rpmbuild -bb $CI_PROJECT_NAME.spec --define "my_version $VERSION" \
	--define "my_release $RELEASE" \
	--define "_sourcedir $CI_PROJECT_DIR" \
	--define "_topdir $CI_PROJECT_DIR"

# EOB
