summer
======

`summer` is a generic checksumming tool. It can calculate multiple checksums
in parallel.

```
usage: summer [-a ALGO] FILENAME [..]

  -h, --help               show this information
  -a, --algo=ALGO          select checksum algorithm
  -T, --files-from=FILE    read list of input files from FILE
  -0, --null, --from0      filenames in list are delimited by NUL characters
  -d, --directio           use direct I/O
  -v                       show version number

Default algorithm is md5. Multiple algorithms may be given.
Supported algorithms are:
  crc32 adler32 md5 sha1 sha224 sha256 sha384 sha512
```

Note that direct I/O is only available if the operating system offers `O_DIRECT`.


The supplied `summer.spec` file was made for SLES12. Your mileage may vary.


Copyright (c) 2016 SURFsara B.V.
