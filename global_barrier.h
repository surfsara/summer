//
//  summer  WJ118
//  global_barrier.h
//
//  Copyright (c) 2018 SURFsara B.V.
//
/*
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef GLOBAL_BARRIER_H_WJ118
#define GLOBAL_BARRIER_H_WJ118  1

#include <mutex>
#include <condition_variable>

class global_barrier {
    std::mutex mtx;
    std::condition_variable cv;
    int waiting, num_threads;

public:
    global_barrier() : mtx(), cv(), waiting(0), num_threads(0) { }
    global_barrier(int n) : mtx(), cv(), waiting(0), num_threads(n) { }

    global_barrier(const global_barrier&) = delete;
    global_barrier(global_barrier&& o) = delete;

    ~global_barrier() { }

    global_barrier& operator=(const global_barrier&) = delete;
    global_barrier& operator=(global_barrier&&) = delete;

    void init(int n) {
        num_threads = n;
        waiting = 0;
    }

    void wait(void) {
        std::unique_lock<std::mutex> lk(mtx);   // lock mutex
        if (waiting >= num_threads) {
            // reusable barrier
            waiting = 1;
        } else {
            waiting++;
        }
        cv.wait(lk, [&]{ return waiting >= num_threads; });
        lk.unlock();

        // wake up everybody
        cv.notify_all();
    }
};

#endif  // GLOBAL_BARRIER_H_WJ118

// EOB
