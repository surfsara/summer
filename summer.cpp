/*
	summer	WJ118
	summer.cpp

	Copyright (c) 2016 SURFsara B.V.

	- calculates multiple checksums in parallel
	- compile with: g++ -Wall -std=c++11 summer.c -lssl -lz -o summer

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _GNU_SOURCE
// this gets O_DIRECT defined
#define _GNU_SOURCE
#endif

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <thread>

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <zlib.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

#include "global_barrier.h"

#ifndef O_DIRECT
// issue a warning
#pragma message ("this platform does not support O_DIRECT")
#endif

const char *version = "1.2";

const int MAX_DIGEST_LEN = SHA512_DIGEST_LENGTH * 2 + 1;

const char *DEFAULT_ALGO = "md5";

typedef struct {
	const char *name;
	void (*func)(void);				// thread function pointer
	char digest[MAX_DIGEST_LEN];	// hash/checksum as text string
} Algorithm;

void thread_crc32(void);
void thread_adler32(void);
void thread_md5sum(void);
void thread_sha1sum(void);
void thread_sha224sum(void);
void thread_sha256sum(void);
void thread_sha384sum(void);
void thread_sha512sum(void);

// enumerations
enum {
	ALGO_CRC32 = 0,
	ALGO_ADLER32,
	ALGO_MD5,
	ALGO_SHA1,
	ALGO_SHA224,
	ALGO_SHA256,
	ALGO_SHA384,
	ALGO_SHA512
};

Algorithm algorithms[] = {
	{ "crc32",		thread_crc32,		"",	},
	{ "adler32",	thread_adler32,		"",	},
	{ "md5",		thread_md5sum,		"",	},
	{ "sha1",		thread_sha1sum,		"",	},
	{ "sha224",		thread_sha224sum,	"",	},
	{ "sha256",		thread_sha256sum,	"",	},
	{ "sha384",		thread_sha384sum,	"",	},
	{ "sha512",		thread_sha512sum,	"",	},
};

const size_t IOSIZE = 64 * 1024 * 1024;

extern char *optarg;
extern int optind;

// options: bit flags
#ifdef O_DIRECT
const int OPT_DIRECT_IO = 1;
#endif
const int OPT_NULL = 2;

int options = 0;
int opt_algo = 0;
int num_threads = 0;
char *arg_files_from = nullptr;

std::thread *all_threads = nullptr;

global_barrier barrier_ready, barrier_calc;

// global, shared I/O buffer
unsigned char *databuf = nullptr;
size_t datalen = 0;


void sprint_digest(char *buf, const unsigned char *digest, int len) {
	int buflen = MAX_DIGEST_LEN;

	while(len > 0) {
		snprintf(buf, buflen, "%02x", *digest);
		buf++;
		buf++;
		buflen -= 2;
		if (buflen <= 1) {
			break;
		}
		digest++;
		len--;
	}
}

void thread_crc32(void) {
	uLong crc32_digest = crc32(0, Z_NULL, 0);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		crc32_digest = crc32(crc32_digest, databuf, datalen) & 0xffffffffUL;

		barrier_ready.wait();
	}

	snprintf(algorithms[ALGO_CRC32].digest, MAX_DIGEST_LEN, "%08lx", crc32_digest);

	barrier_ready.wait();
}

void thread_adler32(void) {
	uLong adler32_digest = adler32(0, Z_NULL, 0);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		adler32_digest = adler32(adler32_digest, databuf, datalen) & 0xffffffffUL;

		barrier_ready.wait();
	}

	snprintf(algorithms[ALGO_ADLER32].digest, MAX_DIGEST_LEN, "%08lx", adler32_digest);

	barrier_ready.wait();
}

void thread_md5sum(void) {
	MD5_CTX md5_context;

	MD5_Init(&md5_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		MD5_Update(&md5_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char md5_digest[MD5_DIGEST_LENGTH];
	MD5_Final(md5_digest, &md5_context);

	sprint_digest(algorithms[ALGO_MD5].digest, md5_digest, MD5_DIGEST_LENGTH);

	barrier_ready.wait();
}

void thread_sha1sum(void) {
	SHA_CTX sha1_context;

	SHA1_Init(&sha1_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		SHA1_Update(&sha1_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char sha1_digest[SHA_DIGEST_LENGTH];
	SHA1_Final(sha1_digest, &sha1_context);

	sprint_digest(algorithms[ALGO_SHA1].digest, sha1_digest, SHA_DIGEST_LENGTH);

	barrier_ready.wait();
}

void thread_sha224sum(void) {
	SHA256_CTX sha224_context;

	SHA224_Init(&sha224_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		SHA224_Update(&sha224_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char sha224_digest[SHA224_DIGEST_LENGTH];
	SHA224_Final(sha224_digest, &sha224_context);

	sprint_digest(algorithms[ALGO_SHA224].digest, sha224_digest, SHA224_DIGEST_LENGTH);

	barrier_ready.wait();
}

void thread_sha256sum(void) {
	SHA256_CTX sha256_context;

	SHA256_Init(&sha256_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		SHA256_Update(&sha256_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char sha256_digest[SHA256_DIGEST_LENGTH];
	SHA256_Final(sha256_digest, &sha256_context);

	sprint_digest(algorithms[ALGO_SHA256].digest, sha256_digest, SHA256_DIGEST_LENGTH);

	barrier_ready.wait();
}

void thread_sha384sum(void) {
	SHA512_CTX sha384_context;

	SHA384_Init(&sha384_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		SHA384_Update(&sha384_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char sha384_digest[SHA384_DIGEST_LENGTH];
	SHA384_Final(sha384_digest, &sha384_context);

	sprint_digest(algorithms[ALGO_SHA384].digest, sha384_digest, SHA384_DIGEST_LENGTH);

	barrier_ready.wait();
}

void thread_sha512sum(void) {
	SHA512_CTX sha512_context;

	SHA512_Init(&sha512_context);

	barrier_ready.wait();

	while(true) {
		barrier_calc.wait();

		if (!datalen) {
			break;
		}
		SHA512_Update(&sha512_context, databuf, datalen);

		barrier_ready.wait();
	}

	unsigned char sha512_digest[SHA512_DIGEST_LENGTH];
	SHA512_Final(sha512_digest, &sha512_context);

	sprint_digest(algorithms[ALGO_SHA512].digest, sha512_digest, SHA512_DIGEST_LENGTH);

	barrier_ready.wait();
}

void report(const char *filename) {
	if (!strcmp(filename, "-")) {
		filename = "/dev/stdin";
	}

	int num_algorithms = sizeof(algorithms) / sizeof(Algorithm);
	for(int i = 0; i < num_algorithms; i++) {
		if (opt_algo & (1 << i)) {
			printf("%s %s %s\n", algorithms[i].digest, algorithms[i].name, filename);
		}
	}
}

bool init(void) {
	// allocate (global) data buffer
	// This data buffer is read by all checksumming threads
#ifdef O_DIRECT
	if (options & OPT_DIRECT_IO) {
		// O_DIRECT requires aligned memory buffer
		int pagesize = getpagesize();
		assert(pagesize > 0);
		databuf = nullptr;
		if (posix_memalign((void **)&databuf, (size_t)pagesize, IOSIZE) != 0) {
			fprintf(stderr, "error: failed to get neatly aligned memory\n");
			return false;
		}
	} else {
		databuf = new unsigned char[IOSIZE];
	}
#else
	// no direct I/O available
	databuf = new unsigned char[IOSIZE];
#endif

	// initialize barriers: +1 because main thread also waits on barrier
	barrier_ready.init(num_threads + 1);
	barrier_calc.init(num_threads + 1);

	// allocate threads
	all_threads = new std::thread[num_threads];

	return true;
}

void deinit(void) {
	delete [] all_threads;
	all_threads = nullptr;

#ifdef O_DIRECT
	if (options & OPT_DIRECT_IO) {
		if (databuf != nullptr) {
			free(databuf);
		}
	} else {
		delete [] databuf;
	}
#else
	delete [] databuf;
#endif
	databuf = nullptr;
}

void launch_threads(void) {
	// now fire up threads for selected algorithms
	int n = 0;
	int num_algorithms = sizeof(algorithms) / sizeof(Algorithm);
	for(int i = 0; i < num_algorithms; i++) {
		if (opt_algo & (1 << i)) {
			assert(n < num_threads);
			all_threads[n++] = std::thread(algorithms[i].func);
		}
	}
}

void join_threads(void) {
	// go join all threads
	barrier_ready.wait();

	for(int i = 0; i < num_threads; i++) {
		all_threads[i].join();
	}
}

bool checksum_file(const char *filename) {
	int flags = O_RDONLY;
#ifdef O_DIRECT
	if (options & OPT_DIRECT_IO) {
		flags |= O_DIRECT;
	}
#endif
	int fd;
	if (!strcmp(filename, "-")) {
		filename = "/dev/stdin";
		fd = fileno(stdin);
	} else {
		fd = open(filename, flags);
	}
	if (fd == -1) {
		fprintf(stderr, "error: failed to open %s: %s\n", filename, strerror(errno));
		return false;
	}

	while(true) {
		barrier_ready.wait();

		datalen = (size_t)read(fd, databuf, IOSIZE);
		if (!datalen) {
			// EOF reached
			// let threads exit
			barrier_calc.wait();
			break;
		}
		if (datalen == (size_t)-1) {
			fprintf(stderr, "error: %s: read error: %s\n", filename, strerror(errno));
			close(fd);
			return false;
		}

		// let threads calculate
		barrier_calc.wait();
	}
	close(fd);
	return true;
}

int add_algorithm(const char *algo) {
	// set bit for algorithm in opt_algo
	int num_algorithms = sizeof(algorithms) / sizeof(Algorithm);
	for(int i = 0; i < num_algorithms; i++) {
		if (!strcasecmp(algo, algorithms[i].name)) {
			if (algorithms[i].func == NULL) {
				fprintf(stderr, "algorithm %s is not yet implemented\n", algo);
				return -1;
			}
			opt_algo |= (1 << i);
			return 0;
		}
	}
	fprintf(stderr, "unknown algorithm '%s'\n", algo);
	return -1;
}

int count_algorithms(void) {
int n = 0;

	// simply count the bits set in opt_algo
	int mask = 1;
	for(unsigned int i = 0; i < sizeof(int) * 8; i++) {
		if (opt_algo & mask) {
			n++;
		}
		mask <<= 1;
	}
	return n;
}

char *fgets_null(char *result, char *buf, size_t sz, FILE *f) {
	if (!*buf) {
		size_t n = fread(buf, 1, sz - 1, f);
		if (!n) {
			// reached EOF
			return nullptr;
		}
		buf[n] = 0;
	}

	size_t l = strlen(buf) + 1;
	strncpy(result, buf, l);

	// shift the read buffer
	memmove(buf, buf + l, sz - l);
	memset(buf + sz - l, 0, l);

	return result;
}

void lstrip(char *buf) {
	// strip trailing newlines (not spaces!)
	int l = strlen(buf) - 1;
	while(l >= 0 && (buf[l] == '\n' || buf[l] == '\r')) {
		buf[l--] = '\0';
	}
}

bool checksum_files_from(const char *files_from) {
	FILE *f;
	if (!strcmp(files_from, "-")) {
		files_from = "/dev/stdin";
		// explicitly request binary mode
		f = fdopen(fileno(stdin), "rb");
	} else {
		f = fopen(files_from, "rb");
	}
	if (f == nullptr) {
		fprintf(stderr, "error: failed to open %s\n", files_from);
		return false;
	}

	bool ok = true;
	struct stat statbuf;
	char filename[4096], linebuf[4096];

	// Note: this is required for fgets_null() to work correctly
	linebuf[0] = 0;

	while(true) {
		if (options & OPT_NULL) {
			if (fgets_null(filename, linebuf, sizeof(linebuf), f) == nullptr) {
				break;
			}
		} else {
			if (fgets(filename, sizeof(filename), f) == nullptr) {
				break;
			}
			lstrip(filename);
		}

		if (!*filename) {
			continue;
		}

		// check whether it's a regular file
		if (stat(filename, &statbuf) == -1) {
			fprintf(stderr, "%s: no such file\n", filename);
			continue;
		}
		if ((statbuf.st_mode & S_IFDIR) == S_IFDIR) {
			fprintf(stderr, "%s: is a directory\n", filename);
			continue;
		}
		if ((statbuf.st_mode & S_IFREG) != S_IFREG) {
			fprintf(stderr, "%s: not a regular file\n", filename);
			continue;
		}

		launch_threads();
		if (!checksum_file(filename)) {
			// error message already printed
			ok = false;
			break;
		}
		join_threads();
		report(filename);
	}

	// Note: this may close stdin. But don't mind
	fclose(f);
	return ok;
}

void usage(const char *progname) {
	printf("usage: %s [-a ALGO] FILENAME [..]\n"
		"\n"
		"  -h, --help               show this information\n"
		"  -a, --algo=ALGO          select checksum algorithm\n"
		"  -T, --files-from=FILE    read list of input files from FILE\n"
		"  -0, --null, --from0      filenames in list are delimited by NUL characters\n"
#ifdef O_DIRECT
		"  -d, --directio           use direct I/O\n"
#endif
		"  -v                       show version number\n"
		"\n"
		"Default algorithm is %s. Multiple algorithms may be given.\n"
		"Supported algorithms are:\n"
		" ", progname, DEFAULT_ALGO);

	// print list of supported algorithms
	int num_algorithms = sizeof(algorithms) / sizeof(Algorithm);
	for(int i = 0; i < num_algorithms; i++) {
		printf(" %s", algorithms[i].name);
	}
	printf("\n\n"
		"Copyright (c) 2016 SURFsara B.V.\n");
	exit(1);
}

int get_options(int argc, char *argv[]) {
	static const struct option longopts[] = {
		{ "help",		no_argument,		nullptr, 'h' },
		{ "algo",		required_argument,	nullptr, 'a' },
		{ "files-from",	required_argument,	nullptr, 'T' },
		{ "from0",		no_argument,		nullptr, '0' },
		{ "null",		no_argument,		nullptr, '0' },
#ifdef O_DIRECT
		{ "directio",	no_argument,		nullptr, 'd' },
#endif
		{ "version",	no_argument,		nullptr, 'v' },
		{ nullptr,		0,					nullptr, 0 }
	};

	int opt;
	while((opt = getopt_long(argc, argv, "ha:T:0sv"
#ifdef O_DIRECT
	"d"
#endif
			, longopts, nullptr)) != -1) {

		switch(opt) {
			case 'h':
				usage(basename(argv[0]));
				// usage() will exit the program
				break;

			case 'a':
				if (add_algorithm(optarg) != 0) {
					exit(1);
				}
				break;

			case 'T':
				arg_files_from = optarg;
				break;

			case '0':
				options |= OPT_NULL;
				break;

#ifdef O_DIRECT
			case 'd':
				options |= OPT_DIRECT_IO;
				break;
#endif

			case 'v':
				printf("summer version %s\n"
					"Copyright (c) 2016 SURFsara B.V.\n", version);
				exit(0);

			default:
				printf("usage: %s [-a ALGO] FILENAME\n", basename(argv[0]));
				exit(1);
		}
	}
	num_threads = count_algorithms();
	if (num_threads <= 0) {
		// select default algorithm
		add_algorithm(DEFAULT_ALGO);
		num_threads = 1;
	}
	if (optind >= argc && arg_files_from == nullptr) {
		fprintf(stderr, "%s: missing filename\n", basename(argv[0]));
		exit(1);
	}
	// filename argument is at argv[optind]
	return 0;
}

int main(int argc, char *argv[]) {
	if (get_options(argc, argv) != 0) {
		return -1;
	}

	if (!init()) {
		return -1;
	}

	if (arg_files_from != nullptr) {
		if (!checksum_files_from(arg_files_from)) {
			exit(-1);
		}
	}

	// first filename is at argv[optind]
	for(int i = optind; i < argc; i++) {
		const char *filename = argv[i];

		launch_threads();
		if (!checksum_file(filename)) {
			return -1;
		}
		join_threads();
		report(filename);
	}
	deinit();
	return 0;
}

// EOB
