#
# summer.spec
#

Name:           summer
Version:        %{my_version}
Release:        %{my_release}
License:        MIT
Summary:        checksumming tool
Url:            https://gitlab.com/surfsara/summer/
Group:          Utilities
BuildRequires:  make gcc-c++ libopenssl-devel zlib-devel
Requires:       libopenssl1_0_0 libz1
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%define sourceurl https://gitlab.com/surfsara/summer.git

%description
summer calculates checksums of files. It supports the most common checksumming
algorithms. Multiple checksums can be calculated in parallel. It supports
direct I/O on platforms that have it.

%prep

%build
make -C %{_sourcedir}

%install
cd %{_sourcedir}
%{__install} -m 755 -D summer %{buildroot}%{_bindir}/summer
%{__install} -m 644 -D LICENSE.txt %{buildroot}%{_defaultdocdir}/%{name}-%{version}/LICENSE.txt

%files
%defattr(-,root,root, 0755)
%{_bindir}/summer
%license %{_defaultdocdir}/%{name}-%{version}/LICENSE.txt

